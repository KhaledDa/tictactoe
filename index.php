<?php

function sendLogo() {
    echo("\033[0;34m
            ████████╗██╗ ██████╗████████╗ █████╗  ██████╗████████╗ ██████╗ ███████╗
            ╚══██╔══╝██║██╔════╝╚══██╔══╝██╔══██╗██╔════╝╚══██╔══╝██╔═══██╗██╔════╝
               ██║   ██║██║        ██║   ███████║██║        ██║   ██║   ██║█████╗  
               ██║   ██║██║        ██║   ██╔══██║██║        ██║   ██║   ██║██╔══╝  
               ██║   ██║╚██████╗   ██║   ██║  ██║╚██████╗   ██║   ╚██████╔╝███████╗
               ╚═╝   ╚═╝ ╚═════╝   ╚═╝   ╚═╝  ╚═╝ ╚═════╝   ╚═╝    ╚═════╝ ╚══════╝
            -----------------------------------------------------------------------
            \033[0;34m|
            \033[0;34m| V.1 | \033[0;32mBy Khaled\033[0m                                       
            \033[0;34m| \033[93mO is computer\033[0m                                           
            \033[0;34m| \033[0;35mX is you\033[0m                                              
            \033[0;34m|                                                             
            -----------------------------------------------------------------------
            \n\n\n\n\n\n\n\n
            ");                            
}

function sendText() {
    $text = readline("\033[0;35m? Do you wanna Start?\033[0m\n \033[0;31m-> ");
    if ($text == "yes" or $text == "y" or $text == "Yes") {
        printBoard([]);
        setPlayer([]);
    }
}

function printBoard(array $lines) {
    $board = "";
    $count = 1;
    for ($i = 0; $i < 3; $i++) {
        $board .= "\033[2;34m-------------------\n";
        for ($x = 0; $x < 3; $x++) {
            if (isset($lines[$count]) and !empty($lines[$count])) {
                $board .= "|  ".$lines[$count]."  \033[2;34m";
                $count++;
            } else {
                $board .= "|  {$count}  ";
                $count++;
            }
        }
        $board .= "|\n";
    }
    $board .= "-------------------\n";
    echo($board);
}

function setPlayer(array $line) {
    $number = readline("\033[0;32mType your number > \033[0m");
    if ($number < 1 or $number > 9) {
        echo("\033[0;31mNumber is out of reach! Pick a nother one! \033[0m\n");
        setPlayer($line);
    } else {
        $line[$number] = "\033[0;35mX\033[0m";
        checkWinner($line);
        printBoard($line);
        echo("\033[0;93mComputer is now Choosing...\033[0m\n");
        sleep(1);
        setComputer($line);
    }
}

function setComputer(array $line) {
    $count = 9;
    if (isset($line[1]) and isset($line[2]) and isset($line[3]) and isset($line[4]) and isset($line[5]) and isset($line[6]) and isset($line[7]) and isset($line[8]) and isset($line[9])) {
        gameEnd();
    }
    $mark = "\033[0;93mO\033[0m";

    $chance = getPossibilities($line);
    $line[$chance] = $mark;
    checkWinner($line);
    printBoard($line);
    setPlayer($line);
}

function checkWinner(array $line) {
    $boardPlayer = [];
    $boardComputer = [];
    foreach ($line as $num => $player) {
        if ($player == "\033[0;35mX\033[0m") {
            $boardPlayer[$num] = "X";
        } else if ($player == "\033[0;93mO\033[0m") {
            $boardComputer[$num] = "X";
        }
    }
    $player = getWinnerPattern($boardPlayer);
    $computer = getWinnerPattern($boardComputer);
    if ($player) {
        echo("🎉 Player won! 🎉\n");
        sleep(2);
        celebrate("player");
    } else if ($computer) {
        echo("🎉 Computer won! 🎉\n");
        sleep(2);
        celebrate("computer");
    } else if ($player and $computer) {
        // TODO
    }
}

function celebrate(string $winner) {
    if ($winner == "player") {
        echo("\n\n\n\n\n\n\033[0;35m
██████╗ ██╗      █████╗ ██╗   ██╗███████╗██████╗     ██╗    ██╗ ██████╗ ███╗   ██╗
██╔══██╗██║     ██╔══██╗╚██╗ ██╔╝██╔════╝██╔══██╗    ██║    ██║██╔═══██╗████╗  ██║
██████╔╝██║     ███████║ ╚████╔╝ █████╗  ██████╔╝    ██║ █╗ ██║██║   ██║██╔██╗ ██║
██╔═══╝ ██║     ██╔══██║  ╚██╔╝  ██╔══╝  ██╔══██╗    ██║███╗██║██║   ██║██║╚██╗██║
██║     ███████╗██║  ██║   ██║   ███████╗██║  ██║    ╚███╔███╔╝╚██████╔╝██║ ╚████║
╚═╝     ╚══════╝╚═╝  ╚═╝   ╚═╝   ╚══════╝╚═╝  ╚═╝     ╚══╝╚══╝  ╚═════╝ ╚═╝  ╚═══╝
            \033[0m\n\n\n\n");
    } else if ($winner == "computer") {
        echo("\n\n\n\n\n\n\033[93m
 ██████╗ ██████╗ ███╗   ███╗██████╗ ██╗   ██╗████████╗███████╗██████╗   ██╗    ██╗ ██████╗ ███╗   ██╗
██╔════╝██╔═══██╗████╗ ████║██╔══██╗██║   ██║╚══██╔══╝██╔════╝██╔══██╗  ██║    ██║██╔═══██╗████╗  ██║
██║     ██║   ██║██╔████╔██║██████╔╝██║   ██║   ██║   █████╗  ██████╔╝  ██║ █╗ ██║██║   ██║██╔██╗ ██║
██║     ██║   ██║██║╚██╔╝██║██╔═══╝ ██║   ██║   ██║   ██╔══╝  ██╔══██╗  ██║███╗██║██║   ██║██║╚██╗██║
╚██████╗╚██████╔╝██║ ╚═╝ ██║██║     ╚██████╔╝   ██║   ███████╗██║  ██║  ╚███╔███╔╝╚██████╔╝██║ ╚████║
 ╚═════╝ ╚═════╝ ╚═╝     ╚═╝╚═╝      ╚═════╝    ╚═╝   ╚══════╝╚═╝  ╚═╝   ╚══╝╚══╝  ╚═════╝ ╚═╝  ╚═══╝
            \033[0m\n\n\n\n\n");
    }
    sleep(5);
    exit();
}

function gameEnd() {
    echo("!-> ❌ Nobody have won! ❌\n");
    exit();
}

function getChances() : array {
    $chances = array([1, 2, 3], [1, 4, 7], [7, 8, 9], [4, 5, 6], [3, 6, 9], [3, 5, 7], [1, 5, 9], [2, 5, 8]);
    return $chances;
}

function getPossibilities(array $line) {
    $boardComputer = [];
    foreach ($line as $num => $player) {
        if ($player == "\033[0;93mO\033[0m") {
            $boardComputer[$num] = "X";
        }
    }
    $lastPattern = 0;
    
    if (!$lastPattern == null) {
        if (!isset($line[getChances()[$lastPattern][0]], $line[getChances()[$lastPattern][1]], $line[getChances()[$lastPattern][2]])) {
            for ($left = 0; $left <= 2; $left++) {
                if (!isset($line[getChances()[$lastPattern][$left]])) {
                    $lastPattern = $pattern + 1;
                    return $line[getChances()[$lastPattern][$left]];
                }
            }
        }
    }
    foreach (getChances() as $pattern => $num) {
        for ($left = 0; $left <= 2; $left++) {
            if (!isset($line[$num[$left]])) {
                $lastPattern = $pattern + 1;
                return $num[$left];
            }
        }
    }
}

function getWinnerPattern(array $board) {
    $chance1 = [1,
        2,
        3];
    $chance2 = [1,
        4,
        7];
    $chance3 = [7,
        8,
        9];
    $chance4 = [4,
        5,
        6];
    $chance5 = [3,
        6,
        9];
    $chance6 = [3,
        5,
        7];
    $chance7 = [1,
        5,
        9];
    $chance8 = [2,
        5,
        8];
    if (isset($board[$chance1[0]], $board[$chance1[1]], $board[$chance1[2]]) or
        isset($board[$chance2[0]], $board[$chance2[1]], $board[$chance2[2]]) or
        isset($board[$chance3[0]], $board[$chance3[1]], $board[$chance3[2]]) or
        isset($board[$chance4[0]], $board[$chance4[1]], $board[$chance4[2]]) or
        isset($board[$chance5[0]], $board[$chance5[1]], $board[$chance5[2]]) or
        isset($board[$chance6[0]], $board[$chance6[1]], $board[$chance6[2]]) or
        isset($board[$chance7[0]], $board[$chance7[1]], $board[$chance7[2]]) or
        isset($board[$chance8[0]], $board[$chance8[1]], $board[$chance8[2]])) {
        return true;
    }
    return false;
}

system("clear");
sendLogo();
sendText();
